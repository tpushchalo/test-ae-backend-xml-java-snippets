package com.agileengine.impl.algorithm;

import com.agileengine.Main;
import com.agileengine.api.ElementAttribute;
import com.agileengine.api.algorithm.IPathSearchingAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class PathSearchingAlgorithmHtml implements IPathSearchingAlgorithm {
    public Collection<String> findElementsPathsInChangedFile(String changedFilePath, Elements searchElements) {
        List<List<ElementAttribute>> originalElementsAttributes = findAllAttributes(searchElements);

        File changedFile = new File(changedFilePath);

        try {
            Document doc = Jsoup.parse(
                    changedFile,
                    Main.CHARSET_NAME,
                    changedFile.getAbsolutePath());


            //TODO: get element by id
            //TODO: get elements by classes
            //TODO: get elements by type etc...

        } catch (IOException e) {
            log.error("Error reading [{}] file", changedFile.getAbsolutePath(), e);
            return Collections.emptyList();
        }
        return null;
    }

    private List<List<ElementAttribute>> findAllAttributes(Elements elements) {
        return elements
                .stream()
                .map(button -> button.attributes().asList()
                        .stream()
                        .map(attr -> getElementAttribute(attr.getKey(), attr.getValue()))
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
    }

    private ElementAttribute getElementAttribute(String elementName, String elementValue) {
        ElementAttribute elementAttribute = ElementAttribute.OTHER;
        try {
            elementAttribute = ElementAttribute.valueOf(elementName);
        } catch (IllegalArgumentException e) {
        }

        elementAttribute.setValue(elementValue);
        return elementAttribute;
    }
}
