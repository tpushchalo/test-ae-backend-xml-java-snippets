package com.agileengine.impl.criteria;

import com.agileengine.Main;
import com.agileengine.api.criteria.IFindByCriteria;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

@Setter
@Slf4j
public class FindByIdCriteria implements IFindByCriteria {

    private String targetElementId;

    @Override
    public Elements findElementsByCriteria(String htmlFilePath) {
        File htmlFile = new File(htmlFilePath);

        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    Main.CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return new Elements(doc.getElementById(targetElementId));

        } catch (IOException e) {
            log.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return null;
        }
    }

    @Override
    public void setCriteriaKey(String key) {
        this.targetElementId = key;
    }
}
