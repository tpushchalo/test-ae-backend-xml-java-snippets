package com.agileengine.impl.criteria;

public enum CriteriaType {
    CSS_SELECT, FIND_BY_ID
}
