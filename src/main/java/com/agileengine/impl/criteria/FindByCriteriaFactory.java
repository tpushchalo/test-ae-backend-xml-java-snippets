package com.agileengine.impl.criteria;

import com.agileengine.api.criteria.IFindByCriteria;

public class FindByCriteriaFactory {
    // you can use any dependency injection pattern here
    private IFindByCriteria cssSelectCriteria = new CssSelectCriteria();
    private IFindByCriteria findByIdCriteria = new FindByIdCriteria();

    public IFindByCriteria getCriteria(CriteriaType criteriaType) {
        IFindByCriteria criteria = null;
        switch (criteriaType) {
            case CSS_SELECT: {
                criteria = cssSelectCriteria;
                break;
            }
            case FIND_BY_ID: {
                criteria = findByIdCriteria;
                break;
            }
            // you can return any default criteria or throw exception
        }
        return criteria;
    }
}
