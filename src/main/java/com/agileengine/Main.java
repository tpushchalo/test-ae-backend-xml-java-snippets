package com.agileengine;

import com.agileengine.api.algorithm.IPathSearchingAlgorithm;
import com.agileengine.api.criteria.IFindByCriteria;
import com.agileengine.impl.algorithm.PathSearchingAlgorithmHtml;
import com.agileengine.impl.criteria.CriteriaType;
import com.agileengine.impl.criteria.FindByCriteriaFactory;
import org.jsoup.select.Elements;

import java.util.Collection;

public class Main {

    public static String CHARSET_NAME = "utf8";

    public static void main(String[] args) {
        // TODO: add checks for null, empty strings etc in order to avoid ArrayOutOfBounds
        String originalFilePath = args[0];
        String diffFilePath = args[1];
        String key = args[2];
        CriteriaType criteriaType = CriteriaType.values()[Integer.valueOf(args[3])];

        FindByCriteriaFactory factory = new FindByCriteriaFactory();
        IFindByCriteria criteria = factory.getCriteria(criteriaType);
        criteria.setCriteriaKey(key);

        Elements originalFileElements = criteria.findElementsByCriteria(originalFilePath);

        IPathSearchingAlgorithm pathSearchingAlgorithm = new PathSearchingAlgorithmHtml();
        Collection<String> elementsPathsInChangedFile = pathSearchingAlgorithm.findElementsPathsInChangedFile(diffFilePath, originalFileElements);

        elementsPathsInChangedFile.forEach(System.out::println);
    }
}
