package com.agileengine.api.criteria;


import org.jsoup.select.Elements;

public interface IFindByCriteria {
    Elements findElementsByCriteria(String filePath);

    void setCriteriaKey(String key);
}
