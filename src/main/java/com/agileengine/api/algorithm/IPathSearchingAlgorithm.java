package com.agileengine.api.algorithm;

import org.jsoup.select.Elements;

import java.util.Collection;

public interface IPathSearchingAlgorithm {
    Collection<String> findElementsPathsInChangedFile(String changedFilePath, Elements searchElements);
}
