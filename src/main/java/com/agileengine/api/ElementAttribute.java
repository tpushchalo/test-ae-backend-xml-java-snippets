package com.agileengine.api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
public enum ElementAttribute {
    ID(1), CLASS(2), TYPE(3), NAME(5), HREF(100), OTHER(100);

    @Setter
    private String value;
    @Getter
    private final int priority;
}
